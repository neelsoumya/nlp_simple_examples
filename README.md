# nlp_simple_examples

Examples showing basics of NLP

Usage:

		* python3 nlp_test.py

Installation:

		* python3
		* user_nltk_dir = "/home/soumya/nltk_data"
		* nltk.download("book", download_dir=user_nltk_dir)

Adapted from:

		* https://github.com/Spaxe/NLTK/blob/contrib/1%20hour%20workshop%20notes.ipynb


